$(function () {
    // 拖放排序
    $('.k-card-deck').kendoSortable({
        filter: '.k-card',
        cursor: 'move',
        placeholder: function (element) {
            return element.clone().css('opacity',  0.5);
        },
        hint: function (element) {
            return element.clone().css({'width': element.width(), 'height': element.height(), 'white-space': 'nowrap'}).removeClass('k-state-selected');
        }
    });
});