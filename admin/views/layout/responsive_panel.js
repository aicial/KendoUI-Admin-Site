$(function () {
    $('#navigation').kendoPanelBar({
        dataSource: navData
    });
    $('#responsivePanel').kendoResponsivePanel({
        toggleButton: '#toggle',
        orientation: 'right',
        breakpoint: 1200
    });
});