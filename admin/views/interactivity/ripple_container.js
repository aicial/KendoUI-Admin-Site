$(function () {
    // 按钮
    $('#buttonRipple').kendoRippleContainer();
    // 列表
    $('#listRipple').kendoRippleContainer({
        elements: [
            {
                selector: 'li'
            }
        ]
    });
});